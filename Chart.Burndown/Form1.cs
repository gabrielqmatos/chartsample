﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Chart.Burndown.Libs.Controllers;
using System.Linq;
using DevExpress.XtraCharts;

namespace Chart.Burndown
{
    public partial class Form1 : DevExpress.XtraEditors.XtraForm
    {
        AnalisadorEstimativas analisador;
        public Form1()
        {
            InitializeComponent();
            LimparGrafico();
            AnalisadorEstimativas.AoConcluirAnalise += AnalisadorEstimativas_AoConcluirAnalise;
            analisador = new AnalisadorEstimativas(dadosControl.Estimavas);
        }

        void AnalisadorEstimativas_AoConcluirAnalise(ResultadoAnalise resultado)
        {
            LimparGrafico();
            chartControl1.Series[0].Points.AddRange(resultado.DadosEstimado.Select(o=> new SeriesPoint(o.Key,o.Value)).ToArray());
            chartControl1.Series[1].Points.AddRange(resultado.DadosRealizado.Select(o => new SeriesPoint(o.Key, o.Value)).ToArray());
        }


        private void LimparGrafico()
        {
            chartControl1.Series[0].Points.Clear();
            chartControl1.Series[1].Points.Clear();
        }

        private void dadosControl_AoAdicionarEstimativa(object sender, EventArgs e)
        {
            analisador.AtualizarAnalise();
        }
    }
}