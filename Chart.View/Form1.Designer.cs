﻿namespace Chart.View
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel1 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions1 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PiePointOptions piePointOptions2 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.SeriesPoint seriesPoint1 = new DevExpress.XtraCharts.SeriesPoint("Masculino", new object[] {
            ((object)(0D))}, 0);
            DevExpress.XtraCharts.SeriesPoint seriesPoint2 = new DevExpress.XtraCharts.SeriesPoint("Feminino", new object[] {
            ((object)(0D))}, 2);
            DevExpress.XtraCharts.PieSeriesView pieSeriesView1 = new DevExpress.XtraCharts.PieSeriesView(new int[0]);
            DevExpress.XtraCharts.SeriesTitle seriesTitle1 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel2 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions3 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView2 = new DevExpress.XtraCharts.PieSeriesView();
            this.colSexo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.lbTotalHomens = new DevExpress.XtraBars.BarStaticItem();
            this.lbTotalMulheres = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.edQuantidadePessoas = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanelChart = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.chartControl = new DevExpress.XtraCharts.ChartControl();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanelGrid = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.pessoaGridControl = new DevExpress.XtraGrid.GridControl();
            this.pessoaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dockPanelGenero = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.chartControlGenero = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            this.dockPanelChart.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            this.panelContainer1.SuspendLayout();
            this.dockPanelGrid.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pessoaGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pessoaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.dockPanelGenero.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControlGenero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).BeginInit();
            this.SuspendLayout();
            // 
            // colSexo
            // 
            this.colSexo.AppearanceCell.Options.UseTextOptions = true;
            this.colSexo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSexo.AppearanceHeader.Options.UseTextOptions = true;
            this.colSexo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSexo.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colSexo.FieldName = "Sexo";
            this.colSexo.Name = "colSexo";
            this.colSexo.OptionsColumn.AllowIncrementalSearch = false;
            this.colSexo.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colSexo.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colSexo.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Sexo", "", "\"Genero\"")});
            this.colSexo.Tag = "Genero";
            this.colSexo.Visible = true;
            this.colSexo.VisibleIndex = 2;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.LargeImages = this.imageCollection1;
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imageCollection1;
            this.repositoryItemImageComboBox1.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.repositoryItemImageComboBox1_Closed);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Sex Female-32.png");
            this.imageCollection1.Images.SetKeyName(1, "Sex Male-32.png");
            this.imageCollection1.Images.SetKeyName(2, "Child-Female-Light-32.png");
            this.imageCollection1.Images.SetKeyName(3, "Child-Male-Light-32.png");
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.Name = "";
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.lbTotalHomens,
            this.lbTotalMulheres,
            this.barButtonItem4,
            this.edQuantidadePessoas});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 8;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbon.Size = new System.Drawing.Size(758, 123);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Adicionar";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Insert);
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Remover";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.LargeGlyph = global::Chart.View.Properties.Resources.User_Delete_32;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Gerar  pessoas";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.LargeGlyph = global::Chart.View.Properties.Resources.Group_Add_32;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // lbTotalHomens
            // 
            this.lbTotalHomens.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lbTotalHomens.Caption = "0";
            this.lbTotalHomens.Glyph = global::Chart.View.Properties.Resources.Child_Male_Light_32;
            this.lbTotalHomens.Id = 4;
            this.lbTotalHomens.LargeGlyph = global::Chart.View.Properties.Resources.User_Male_32;
            this.lbTotalHomens.Name = "lbTotalHomens";
            this.lbTotalHomens.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lbTotalMulheres
            // 
            this.lbTotalMulheres.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lbTotalMulheres.Caption = "0";
            this.lbTotalMulheres.Glyph = global::Chart.View.Properties.Resources.Child_Female_Light_32;
            this.lbTotalMulheres.Id = 5;
            this.lbTotalMulheres.Name = "lbTotalMulheres";
            this.lbTotalMulheres.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Remover Todos";
            this.barButtonItem4.Id = 6;
            this.barButtonItem4.LargeGlyph = global::Chart.View.Properties.Resources.Group_Delete_32;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // edQuantidadePessoas
            // 
            this.edQuantidadePessoas.Caption = "Quantidade";
            this.edQuantidadePessoas.Edit = this.repositoryItemSpinEdit1;
            this.edQuantidadePessoas.EditValue = "100";
            this.edQuantidadePessoas.Id = 7;
            this.edQuantidadePessoas.Name = "edQuantidadePessoas";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Menu";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowMinimize = false;
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem3, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.edQuantidadePessoas);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem4, true);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Pessoas";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.lbTotalHomens);
            this.ribbonStatusBar.ItemLinks.Add(this.lbTotalMulheres);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 510);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(758, 31);
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanelChart,
            this.panelContainer1});
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanelChart
            // 
            this.dockPanelChart.Controls.Add(this.dockPanel1_Container);
            this.dockPanelChart.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanelChart.FloatVertical = true;
            this.dockPanelChart.ID = new System.Guid("492b81e0-8c0d-49c1-ac92-3e04f43db339");
            this.dockPanelChart.Location = new System.Drawing.Point(0, 310);
            this.dockPanelChart.Name = "dockPanelChart";
            this.dockPanelChart.Options.ShowCloseButton = false;
            this.dockPanelChart.OriginalSize = new System.Drawing.Size(269, 200);
            this.dockPanelChart.Size = new System.Drawing.Size(758, 200);
            this.dockPanelChart.Text = "Faixa Etária";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.chartControl);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(750, 173);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // chartControl
            // 
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.NumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl.Diagram = xyDiagram1;
            this.chartControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl.Location = new System.Drawing.Point(0, 0);
            this.chartControl.Name = "chartControl";
            sideBySideBarSeriesLabel1.LineVisible = true;
            pointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            sideBySideBarSeriesLabel1.PointOptions = pointOptions1;
            sideBySideBarSeriesLabel1.ShowForZeroValues = true;
            series2.Label = sideBySideBarSeriesLabel1;
            series2.LegendText = "Faixa Etária";
            series2.Name = "Idade";
            series3.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            sideBySideBarSeriesLabel2.LineVisible = true;
            series3.Label = sideBySideBarSeriesLabel2;
            series3.LegendText = "Sexo";
            series3.Name = "Sexo";
            series3.Visible = false;
            this.chartControl.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2,
        series3};
            sideBySideBarSeriesLabel3.LineVisible = true;
            this.chartControl.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chartControl.Size = new System.Drawing.Size(750, 173);
            this.chartControl.TabIndex = 0;
            // 
            // panelContainer1
            // 
            this.panelContainer1.Controls.Add(this.dockPanelGrid);
            this.panelContainer1.Controls.Add(this.dockPanelGenero);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Top;
            this.panelContainer1.FloatVertical = true;
            this.panelContainer1.ID = new System.Guid("76c05d45-447a-48c7-841f-6abb6718a05b");
            this.panelContainer1.Location = new System.Drawing.Point(0, 123);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.OriginalSize = new System.Drawing.Size(502, 200);
            this.panelContainer1.Size = new System.Drawing.Size(758, 187);
            this.panelContainer1.Text = "panelContainer1";
            // 
            // dockPanelGrid
            // 
            this.dockPanelGrid.Controls.Add(this.controlContainer2);
            this.dockPanelGrid.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelGrid.FloatVertical = true;
            this.dockPanelGrid.ID = new System.Guid("bcfbf2ef-38f5-42a2-bdb9-f73b80930bad");
            this.dockPanelGrid.Location = new System.Drawing.Point(0, 0);
            this.dockPanelGrid.Name = "dockPanelGrid";
            this.dockPanelGrid.Options.AllowFloating = false;
            this.dockPanelGrid.Options.ShowCloseButton = false;
            this.dockPanelGrid.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanelGrid.Size = new System.Drawing.Size(379, 187);
            this.dockPanelGrid.Text = "Pessoas";
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.pessoaGridControl);
            this.controlContainer2.Location = new System.Drawing.Point(4, 23);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(371, 160);
            this.controlContainer2.TabIndex = 0;
            // 
            // pessoaGridControl
            // 
            this.pessoaGridControl.DataSource = this.pessoaBindingSource;
            this.pessoaGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pessoaGridControl.Location = new System.Drawing.Point(0, 0);
            this.pessoaGridControl.MainView = this.gridView1;
            this.pessoaGridControl.MenuManager = this.ribbon;
            this.pessoaGridControl.Name = "pessoaGridControl";
            this.pessoaGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemImageComboBox1});
            this.pessoaGridControl.Size = new System.Drawing.Size(371, 160);
            this.pessoaGridControl.TabIndex = 3;
            this.pessoaGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // pessoaBindingSource
            // 
            this.pessoaBindingSource.DataSource = typeof(Chart.View.Libs.Pessoa);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNome,
            this.colIdade,
            this.colSexo});
            this.gridView1.GridControl = this.pessoaGridControl;
            this.gridView1.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Sexo", null, "", "Genero")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsMenu.EnableFooterMenu = false;
            this.gridView1.HiddenEditor += new System.EventHandler(this.gridView1_HiddenEditor);
            // 
            // colNome
            // 
            this.colNome.FieldName = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.Visible = true;
            this.colNome.VisibleIndex = 0;
            // 
            // colIdade
            // 
            this.colIdade.FieldName = "Idade";
            this.colIdade.Name = "colIdade";
            this.colIdade.Visible = true;
            this.colIdade.VisibleIndex = 1;
            // 
            // dockPanelGenero
            // 
            this.dockPanelGenero.Controls.Add(this.controlContainer1);
            this.dockPanelGenero.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelGenero.FloatVertical = true;
            this.dockPanelGenero.ID = new System.Guid("fff6ee3b-1134-4936-931b-be4b0a80b123");
            this.dockPanelGenero.Location = new System.Drawing.Point(379, 0);
            this.dockPanelGenero.Name = "dockPanelGenero";
            this.dockPanelGenero.Options.ShowCloseButton = false;
            this.dockPanelGenero.OriginalSize = new System.Drawing.Size(379, 187);
            this.dockPanelGenero.Size = new System.Drawing.Size(379, 187);
            this.dockPanelGenero.Text = "Gênero";
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.chartControlGenero);
            this.controlContainer1.Location = new System.Drawing.Point(4, 23);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(371, 160);
            this.controlContainer1.TabIndex = 0;
            // 
            // chartControlGenero
            // 
            this.chartControlGenero.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControlGenero.Location = new System.Drawing.Point(0, 0);
            this.chartControlGenero.Name = "chartControlGenero";
            this.chartControlGenero.PaletteName = "Genero";
            this.chartControlGenero.PaletteRepository.Add("Genero", new DevExpress.XtraCharts.Palette("Genero", DevExpress.XtraCharts.PaletteScaleMode.Repeat, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(192))))), System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(128)))), ((int)(((byte)(192)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(164)))), ((int)(((byte)(209))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(164)))), ((int)(((byte)(209))))))}));
            pieSeriesLabel1.LineVisible = true;
            piePointOptions1.PercentOptions.PercentageAccuracy = 4;
            piePointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            pieSeriesLabel1.PointOptions = piePointOptions1;
            pieSeriesLabel1.Shadow.Visible = true;
            series1.Label = pieSeriesLabel1;
            piePointOptions2.PointView = DevExpress.XtraCharts.PointView.Argument;
            piePointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            series1.LegendPointOptions = piePointOptions2;
            series1.LegendText = "Gênero";
            series1.Name = "SeriesGenero";
            series1.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint1,
            seriesPoint2});
            series1.SynchronizePointOptions = false;
            pieSeriesView1.ExplodeMode = DevExpress.XtraCharts.PieExplodeMode.UsePoints;
            pieSeriesView1.Rotation = 90;
            pieSeriesView1.RuntimeExploding = false;
            seriesTitle1.Text = "Gênero";
            pieSeriesView1.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle1});
            series1.View = pieSeriesView1;
            this.chartControlGenero.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            pieSeriesLabel2.LineVisible = true;
            piePointOptions3.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Percent;
            piePointOptions3.ValueNumericOptions.Precision = 0;
            pieSeriesLabel2.PointOptions = piePointOptions3;
            this.chartControlGenero.SeriesTemplate.Label = pieSeriesLabel2;
            pieSeriesView2.RuntimeExploding = false;
            this.chartControlGenero.SeriesTemplate.View = pieSeriesView2;
            this.chartControlGenero.Size = new System.Drawing.Size(371, 160);
            this.chartControlGenero.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 541);
            this.Controls.Add(this.panelContainer1);
            this.Controls.Add(this.dockPanelChart);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            this.dockPanelChart.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl)).EndInit();
            this.panelContainer1.ResumeLayout(false);
            this.dockPanelGrid.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pessoaGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pessoaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.dockPanelGenero.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControlGenero)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelChart;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private System.Windows.Forms.BindingSource pessoaBindingSource;
        private DevExpress.XtraGrid.GridControl pessoaGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colNome;
        private DevExpress.XtraGrid.Columns.GridColumn colIdade;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colSexo;
        private DevExpress.XtraCharts.ChartControl chartControl;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGenero;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraCharts.ChartControl chartControlGenero;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGrid;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarStaticItem lbTotalHomens;
        private DevExpress.XtraBars.BarStaticItem lbTotalMulheres;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarEditItem edQuantidadePessoas;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
    }
}