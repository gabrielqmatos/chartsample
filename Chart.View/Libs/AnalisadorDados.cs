﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.View.Libs
{
    public class AnalisadorDados
    {
        IList<Pessoa> pessoas;
        public static event Action<List<ResultadoFaixaEtaria>> AoAnalisarFaixaEtaria;
        public static event Action<ResultadoAnaliseGenero> AoAnalisarGenero;
        public AnalisadorDados(IList<Pessoa> lista)
        {
            AtualizarAnalise(lista);
        }

        public void AtualizarAnalise(IList<Pessoa> lista)
        {
            pessoas = lista;
            AtualizarAnalise();
        }

        public void AtualizarAnalise()
        {
            CalcularAnalisePorFaixaEtaria(null);
            AnalisarDadosGenero();
        }

        private void CalcularAnalisePorFaixaEtaria(List<Range> faixasEtarias)
        {
            if (pessoas == null)
                return;

            faixasEtarias = new List<Range>();
            faixasEtarias.Add(new Range(0, 10));
            faixasEtarias.Add(new Range(11, 15));
            faixasEtarias.Add(new Range(16, 22));
            faixasEtarias.Add(new Range(23, 38));
            faixasEtarias.Add(new Range(39, 70));

            int totalPessoas = pessoas.Count;
            faixasEtarias = faixasEtarias.OrderBy(o => o.Min).ToList();
            List<ResultadoFaixaEtaria> resultados = new List<ResultadoFaixaEtaria>();
            foreach (var faixa in faixasEtarias)
            {
                int qtPessoasNaFaixa = pessoas.Count(o => faixa.Contem(o.Idade));
                resultados.Add(new ResultadoFaixaEtaria(faixa, qtPessoasNaFaixa, totalPessoas));
            }
            if (AoAnalisarFaixaEtaria != null)
            {
                AoAnalisarFaixaEtaria(resultados);
            }
        }

        private void AnalisarDadosGenero()
        {
            if (pessoas == null)
                return;

            int totalMulheres = pessoas.Count(o => o.Sexo == Sexo.FEMININO);
            int totalHomens = pessoas.Count - totalMulheres;
            ResultadoAnaliseGenero resultado = new ResultadoAnaliseGenero(pessoas.Count, totalHomens, totalMulheres);
            if (AoAnalisarGenero != null)
            {
                AoAnalisarGenero(resultado);
            }
        }
    }
}
