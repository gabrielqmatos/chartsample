﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.View.Libs
{
    public class Range
    {
        private int min;
        private int max;

        public int Min
        {
            get { return min; }
        }
        public int Max
        {
            get { return max; }
        }

        public Range(int min,int max)
        {
            this.min = min;
            this.max = max;
        }

        public bool Contem(int valor) 
        {
            return valor >= min && valor <= max;
        }
    }
}
