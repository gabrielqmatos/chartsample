﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.View.Libs
{
    public class Pessoa
    {
        public string Nome { get; set; }

        public int Idade { get; set; }

        public Sexo Sexo { get; set; }
    }
}
