﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.Burndown.Libs
{
    [Serializable]
    public class Estimativa
    {
        public DateTime Data { get; set; }
        public int Horas { get; set; }
        public TipoEstimativa Estado { get; set; }
    }
}
