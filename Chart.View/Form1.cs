﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Chart.View.Libs;
using System.Linq;
using DevExpress.XtraCharts;
using System.Threading;
using DevExpress.XtraEditors.Controls;
using DevExpress.Data;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid;

namespace Chart.View
{
    public partial class Form1 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        List<string> nomesMasculinos = new List<string>() { "Gabriel", "Pedro", "Luis", "Marcos" , "Julio" , "Denis" , "Pablo" , "Anderson" };
        List<string> nomesFemininos = new List<string>() { "Samantha", "Paula", "Juliana", "Camila" , "Mariana" , "Andreza" , "Priscila" };
        BindingList<Pessoa> pessoas;
        AnalisadorDados analisador;
        Random aleatorio;
        public Form1()
        {
            InitializeComponent();
            pessoas = new BindingList<Pessoa>();
            pessoaBindingSource.DataSource = pessoas;
            analisador = new AnalisadorDados(null);
            AnalisadorDados.AoAnalisarFaixaEtaria += AnalisadorDados_AoAnalisarFaixaEtaria;
            AnalisadorDados.AoAnalisarGenero += AnalisadorDados_AoAnalisarGenero;
            PreencherComboGenero();
            aleatorio = new Random();
        }

        void AnalisadorDados_AoAnalisarGenero(ResultadoAnaliseGenero resultado)
        {
            chartControlGenero.Series[0].Points.Clear();
            chartControlGenero.Series[0].Points.Add(new SeriesPoint(Sexo.MASCULINO.ToString(), resultado.TotalPercHomens));
            chartControlGenero.Series[0].Points.Add(new SeriesPoint(Sexo.FEMININO.ToString(), resultado.TotalPercMulheres));
            AtualizarHints();
        }

        private void AtualizarHints()
        {
            int total = pessoas.Count;
            int homens = pessoas.Count(o => o.Sexo == Sexo.MASCULINO);
            int mulheres = total - homens;
            lbTotalHomens.Caption = string.Format("{0} - ({1}%)", homens, ResultadoAnaliseGenero.CalcularPorcentagem(total, homens) * 100);
            lbTotalMulheres.Caption = string.Format("{0} - ({1}%)", mulheres, ResultadoAnaliseGenero.CalcularPorcentagem(total, mulheres) * 100);
        }

        void AnalisadorDados_AoAnalisarFaixaEtaria(List<ResultadoFaixaEtaria> obj)
        {
            chartControl.Series[0].Points.Clear();
            chartControl.Series[0].Points.AddRange(obj.Select(o => new SeriesPoint(o.Label, o.TotalPerc)).ToArray());
        }

        private void PreencherComboGenero()
        {
            repositoryItemImageComboBox1.Items.Add(new ImageComboBoxItem(Sexo.MASCULINO.ToString(), Sexo.MASCULINO, 3));
            repositoryItemImageComboBox1.Items.Add(new ImageComboBoxItem(Sexo.FEMININO.ToString(), Sexo.FEMININO, 2));
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            CriarPessoa();
            analisador.AtualizarAnalise(pessoas);
        }

        private void CriarPessoa()
        {
            Sexo sexo = EscolherSexoAleatorio();
            string nome = EscolherNomeAleatorio(sexo);
            pessoas.Add(new Pessoa() { Nome = nome, Idade = aleatorio.Next(0, 70), Sexo = sexo });
        }

        private string EscolherNomeAleatorio(Sexo sexo) 
        {
            List<string> nomes;
            if (sexo == Sexo.MASCULINO)
            {
                nomes = nomesMasculinos;
            }
            else 
            {
                nomes = nomesFemininos;
            }
            if(nomes == null || nomes.Count <= 0)
                return string.Empty;

            return nomes.ElementAt(aleatorio.Next(0, nomes.Count - 1));
        }

        private Sexo EscolherSexoAleatorio()
        {
            int num = aleatorio.Next(0, 10);
            if (num % 2 == 0)
                return Sexo.FEMININO;
            return Sexo.MASCULINO;
        }

        private void gridView1_HiddenEditor(object sender, EventArgs e)
        {
            analisador.AtualizarAnalise(pessoas);
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            int quantidade = Int32.Parse(edQuantidadePessoas.EditValue.ToString());
            if (quantidade <= 0)
            {
                quantidade = 0;
                edQuantidadePessoas.EditValue = 0;
                return;
            }

            for (int i = 0; i < quantidade; i++)
            {
                CriarPessoa();
            }
            analisador.AtualizarAnalise(pessoas);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void repositoryItemImageComboBox1_Closed(object sender, ClosedEventArgs e)
        {
            gridView1.CloseEditor();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            Pessoa p = gridView1.GetFocusedRow() as Pessoa;
            if (p == null)
                return;

            pessoas.Remove(p);
            analisador.AtualizarAnalise();
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            pessoas.Clear();
            analisador.AtualizarAnalise();
        }
    }
}