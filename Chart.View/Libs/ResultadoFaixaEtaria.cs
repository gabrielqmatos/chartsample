﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.View.Libs
{
    public class ResultadoFaixaEtaria
    {
        public string Label { get; private set; }
        public double Quantidade { get; private set; }
        public int Total { get; private set; }
        private double totalPerc;

        public double TotalPerc
        {
            get { return totalPerc; }
        }
        public ResultadoFaixaEtaria(Range rangeFaixaEtaria, int quantidade,int total)
        {
            Label = string.Format("{0} - {1}({2}unit.)", rangeFaixaEtaria.Min, rangeFaixaEtaria.Max,quantidade);
            Quantidade = quantidade;
            Total = total;
            CalcularTotalPerc();
        }

        private void CalcularTotalPerc()
        {
            totalPerc = Quantidade > 0 ? (Quantidade / Total) : 0;
        }
    }
}
