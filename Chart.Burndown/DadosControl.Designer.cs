﻿namespace Chart.Burndown
{
    partial class DadosControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dataLabel;
            System.Windows.Forms.Label horasLabel;
            this.estimativaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.estimativaGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoras = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.estadoComboBox = new System.Windows.Forms.ComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.horasUpDown = new System.Windows.Forms.NumericUpDown();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            dataLabel = new System.Windows.Forms.Label();
            horasLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.estimativaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.estimativaGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.horasUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLabel
            // 
            dataLabel.AutoSize = true;
            dataLabel.Location = new System.Drawing.Point(20, 18);
            dataLabel.Name = "dataLabel";
            dataLabel.Size = new System.Drawing.Size(34, 13);
            dataLabel.TabIndex = 1;
            dataLabel.Text = "Data:";
            // 
            // horasLabel
            // 
            horasLabel.AutoSize = true;
            horasLabel.Location = new System.Drawing.Point(20, 50);
            horasLabel.Name = "horasLabel";
            horasLabel.Size = new System.Drawing.Size(39, 13);
            horasLabel.TabIndex = 5;
            horasLabel.Text = "Horas:";
            // 
            // estimativaBindingSource
            // 
            this.estimativaBindingSource.DataSource = typeof(Chart.Burndown.Libs.Estimativa);
            // 
            // estimativaGridControl
            // 
            this.estimativaGridControl.DataSource = this.estimativaBindingSource;
            this.estimativaGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.estimativaGridControl.Location = new System.Drawing.Point(0, 82);
            this.estimativaGridControl.MainView = this.gridView1;
            this.estimativaGridControl.Name = "estimativaGridControl";
            this.estimativaGridControl.Size = new System.Drawing.Size(463, 287);
            this.estimativaGridControl.TabIndex = 1;
            this.estimativaGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colData,
            this.colHoras,
            this.colEstado});
            this.gridView1.GridControl = this.estimativaGridControl;
            this.gridView1.Name = "gridView1";
            // 
            // colData
            // 
            this.colData.FieldName = "Data";
            this.colData.Name = "colData";
            this.colData.Visible = true;
            this.colData.VisibleIndex = 0;
            // 
            // colHoras
            // 
            this.colHoras.FieldName = "Horas";
            this.colHoras.Name = "colHoras";
            this.colHoras.Visible = true;
            this.colHoras.VisibleIndex = 1;
            // 
            // colEstado
            // 
            this.colEstado.FieldName = "Estado";
            this.colEstado.Name = "colEstado";
            this.colEstado.Visible = true;
            this.colEstado.VisibleIndex = 2;
            // 
            // dataDateTimePicker
            // 
            this.dataDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.estimativaBindingSource, "Data", true));
            this.dataDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataDateTimePicker.Location = new System.Drawing.Point(70, 14);
            this.dataDateTimePicker.Name = "dataDateTimePicker";
            this.dataDateTimePicker.ShowUpDown = true;
            this.dataDateTimePicker.Size = new System.Drawing.Size(84, 21);
            this.dataDateTimePicker.TabIndex = 2;
            // 
            // estadoComboBox
            // 
            this.estadoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.estimativaBindingSource, "Estado", true));
            this.estadoComboBox.FormattingEnabled = true;
            this.estadoComboBox.Location = new System.Drawing.Point(160, 14);
            this.estadoComboBox.Name = "estadoComboBox";
            this.estadoComboBox.Size = new System.Drawing.Size(110, 21);
            this.estadoComboBox.TabIndex = 4;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.horasUpDown);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(dataLabel);
            this.panelControl1.Controls.Add(this.dataDateTimePicker);
            this.panelControl1.Controls.Add(horasLabel);
            this.panelControl1.Controls.Add(this.estadoComboBox);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(463, 82);
            this.panelControl1.TabIndex = 7;
            // 
            // horasUpDown
            // 
            this.horasUpDown.Location = new System.Drawing.Point(70, 48);
            this.horasUpDown.Name = "horasUpDown";
            this.horasUpDown.Size = new System.Drawing.Size(84, 21);
            this.horasUpDown.TabIndex = 8;
            this.horasUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(160, 48);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(110, 23);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Text = "Adicionar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // DadosControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.estimativaGridControl);
            this.Controls.Add(this.panelControl1);
            this.Name = "DadosControl";
            this.Size = new System.Drawing.Size(463, 369);
            ((System.ComponentModel.ISupportInitialize)(this.estimativaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.estimativaGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.horasUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource estimativaBindingSource;
        private DevExpress.XtraGrid.GridControl estimativaGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colData;
        private DevExpress.XtraGrid.Columns.GridColumn colHoras;
        private DevExpress.XtraGrid.Columns.GridColumn colEstado;
        private System.Windows.Forms.DateTimePicker dataDateTimePicker;
        private System.Windows.Forms.ComboBox estadoComboBox;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.NumericUpDown horasUpDown;
    }
}
