﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.Burndown.Libs.Controllers
{
    public class AnalisadorEstimativas
    {
        private IList<Estimativa> estimativas;


        public IList<Estimativa> Estimativas
        {
            get { return estimativas; }
            private set
            {
                if (value != null)
                {
                    estimativas = value;
                    AtualizarAnalise();
                }
            }
        }

        public static event Action<ResultadoAnalise> AoConcluirAnalise;

        public AnalisadorEstimativas(IList<Estimativa> estimativas)
        {
            Estimativas = estimativas;
        }

        public void AtualizarAnalise()
        {
            if (estimativas == null)
                return;

            if (estimativas.Count == 0)
                return;
            double totalRealizado = GetTotalPorTipo(TipoEstimativa.REALIZADO);
            double totalEstimado = GetTotalPorTipo(TipoEstimativa.ESTIMADO);

            Dictionary<DateTime, int> totaisEstimadosDiarios, totaisRealizadosDiarios;
            totaisEstimadosDiarios = GetTotaisDiariosPorTipo(TipoEstimativa.ESTIMADO);
            totaisRealizadosDiarios = GetTotaisDiariosPorTipo(TipoEstimativa.REALIZADO);

            Dictionary<DateTime, double> resultadosEstimado = new Dictionary<DateTime, double>();
            Dictionary<DateTime, double> resultadosRealizado = new Dictionary<DateTime, double>(); ;
            double total = totalEstimado;
            bool first = true;
            foreach (var item in totaisEstimadosDiarios)
            {
                if (first) 
                {
                    resultadosEstimado.Add(item.Key.AddDays(-1),total);
                    first = false;
                }
                total -= item.Value;
                resultadosEstimado.Add(item.Key, total );
            }
            total = totalEstimado;
            first = true;
            foreach (var item in totaisRealizadosDiarios)
            {
                if (first)
                {
                    resultadosRealizado.Add(item.Key.AddDays(-1), total);
                    first = false;
                }
                total -= item.Value;
                resultadosRealizado.Add(item.Key, total );
            }
            ResultadoAnalise resultado = new ResultadoAnalise(resultadosEstimado, resultadosRealizado);
            if (AoConcluirAnalise != null)
            {
                AoConcluirAnalise(resultado);
            }
        }

        private Dictionary<DateTime, int> GetTotaisDiariosPorTipo(TipoEstimativa tipo)
        {
            return estimativas.Where(o => o.Estado == tipo).OrderBy(o=>o.Data).GroupBy(o => o.Data).ToDictionary(o => o.Key, o => o.Sum(x => x.Horas));
        }

        private double GetTotalPorTipo(TipoEstimativa tipo)
        {
            return estimativas.Where(o => o.Estado == tipo).Sum(o => o.Horas);
        }
    }
}
