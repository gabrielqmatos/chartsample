﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Chart.Burndown.Libs;

namespace Chart.Burndown
{
    public partial class DadosControl : DevExpress.XtraEditors.XtraUserControl
    {
        private BindingList<Estimativa> estimavas;

        public BindingList<Estimativa> Estimavas
        {
            get { return estimavas; }
        }

        public event EventHandler AoAdicionarEstimativa;
        public DadosControl()
        {
            InitializeComponent();
            InicializarCombo();
            InicializarGrid();
        }

        private void InicializarGrid()
        {
            estimavas = new BindingList<Estimativa>();
            estimativaGridControl.DataSource = estimavas;
        }

        private void InicializarCombo()
        {
            estadoComboBox.Items.Add(TipoEstimativa.ESTIMADO);
            estadoComboBox.Items.Add(TipoEstimativa.REALIZADO);
            estadoComboBox.SelectedIndex = 0;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Estimativa estimativa = new Estimativa();
            estimativa.Data = dataDateTimePicker.Value.Date;
            estimativa.Estado = (TipoEstimativa)estadoComboBox.SelectedItem;
            estimativa.Horas = horasUpDown.Value > 0 ? Convert.ToInt32(horasUpDown.Value) : 0;
            estimavas.Add(estimativa);
            if (AoAdicionarEstimativa != null)
            {
                AoAdicionarEstimativa(this, new EventArgs());
            }
        }
    }
}
