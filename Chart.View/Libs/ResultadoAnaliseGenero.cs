using System;
using System.Collections.Generic;
using System.Linq;

namespace Chart.View.Libs
{
    public class ResultadoAnaliseGenero
    {
        public double Total { get; private set; }
        public double TotalPercMulheres { get; set; }
        public double TotalPercHomens { get; set; }
        public ResultadoAnaliseGenero(int totalPessoas, int totalHomens, int totalMulheres)
        {
            Total = totalPessoas;
            CalcularPorcentagens(totalPessoas, totalHomens, totalMulheres);
        }

        private void CalcularPorcentagens(int totalPessoas, int totalHomens, int totalMulheres)
        {
            if (totalPessoas <= 0)
            {
                TotalPercHomens = 0;
                TotalPercMulheres = 0;
                return;
            }

            TotalPercMulheres = CalcularPorcentagem(totalPessoas, totalMulheres);
            TotalPercHomens = CalcularPorcentagem(totalPessoas, totalHomens);
        }

        public static double CalcularPorcentagem(double quantidadeTotal, double quantidadeAmostra)
        {
            return (quantidadeTotal > 0) ? quantidadeAmostra / quantidadeTotal : 0;
        }
    }
}
