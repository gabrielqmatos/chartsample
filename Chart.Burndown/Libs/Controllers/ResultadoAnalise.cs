﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chart.Burndown.Libs.Controllers
{
    public class ResultadoAnalise
    {
        public Dictionary<DateTime, double> DadosEstimado { get; private set; }
        public Dictionary<DateTime, double> DadosRealizado { get; private set; }
        public ResultadoAnalise(Dictionary<DateTime, double> estimado,Dictionary<DateTime, double> realizado)
        {
            DadosEstimado = estimado;
            DadosRealizado = realizado;
        }
    }
}
